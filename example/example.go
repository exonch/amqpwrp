package main

import (
	"bitbucket.org/exonch/amqpwrp"
	"fmt"
	"github.com/Sirupsen/logrus"
	"github.com/streadway/amqp"
	"time"
)

func main() {
	rabbit := amqpwrp.RabbitMQConfig{
		Addr:          "46.101.136.235:5672",
		Login:         "user",
		Pass:          "pass",
		ChanName:      "apiOutTest",
		ReconnTimeout: 7,
	}
	log := logrus.New()
	log.Print("Logger")
	rabbit.Init(log)
	err := rabbit.Connect()
	if err != nil {
		log.Errorln(err)
		return
	}
	defer (*rabbit.GetConnectionLink()).Close()
	chLink, err := rabbit.ChannelLink()
	if err != nil {
		log.Errorf("Failed to create channel")
	}
	(*chLink).Publish(
		"",              // exchange
		rabbit.ChanName, // routing key
		true,            // mandatory
		false,           // immediate
		amqp.Publishing{
			ContentType: "application/json",
			Body:        []byte("Mama said knock you out"),
		})

	fmt.Println("Sleeps 30s")
	time.Sleep(time.Second * 30)
	//If you drop internet connection
	err = (*chLink).Publish("", rabbit.ChanName, true, false,
		amqp.Publishing{
			ContentType: "application/json",
			Body:        []byte("I'm gonna knock you out"),
		})
	//Error will appear.
	if err != nil {
		log.Println("Publish err: %v", err)
	}
	fmt.Println("Sleeps 30s more")
	time.Sleep(time.Second * 30)
	//Then if you connect to the internet again. amqpwrp will reconnect to rabbitMQ and will recreate all channels
	(*chLink).Publish("", rabbit.ChanName, true, false,
		amqp.Publishing{
			ContentType: "application/json",
			Body:        []byte("I'm gonna knock you out"),
		})
	//Msg should send successfully
	if err != nil {
		log.Println("Publish err: %v", err)
	}
	fmt.Println("End of programm")
}
