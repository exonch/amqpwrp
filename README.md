# amqp wrapper package #
Package amqpwrp has some wrapped amqp methods which automatically maintain connection and write logs
### ###
* Version 0.0

### How do I get set up? ###

* give me your ssh key
* Write in terminal

```
#!bash

git config --global url."git@bitbucket.org:".insteadOf "https://bitbucket.org/"
go get bitbucket.org/exonch/amqpwrp
```

* Depended on: "github.com/Sirupsen/logrus" "github.com/streadway/amqp"

### Example ###

* Configure rabbitMQ:

```
#!go

rabbit := amqpwrp.RabbitMQConfig{
		Addr:          "46.101.136.235:5672",
		Login:         "user",
		Pass:          "pass",
		ChanName:      "apiOutTest",
		ReconnTimeout: 5, // 7 by default
	}
```
* Initialization:

```
#!go

log := logrus.New()
//log settings
rabbit.Init(log)
```
* Create sustainable channel:(ChannelLink return pointer on *amqp.Channel)

```
#!go

chLink, err := rabbit.ChannelLink()
```

* Sustainable consume(Return chan):

```
#!go

ch := rabbit.SetConsumeMode(make(chan []byte, msgBuffer))
```
 //Realized only one sust. consume for one connection yet. Consume RabbitMQConfig.ChanName queue