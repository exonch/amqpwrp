//Package amqpwrp has some wrapped amqp methods
//which automatically maintain connection and write logs
package amqpwrp

import (
	"fmt"
	"github.com/Sirupsen/logrus"
	"github.com/streadway/amqp"
	"sync"
	"time"
)

type RabbitMQConfig struct {
	Addr           string `json:"addr"`
	Login          string `json:"login"`
	Pass           string `json:"pass"`
	ChanName       string `json:"chanName"`
	ReconnTimeout  uint   `json:"reconnTimeout"`
	connectionLink **amqp.Connection
	channelLinks   []**amqp.Channel
	ConsumeMode    bool `json:consumeMode`
	ConsumeChan    chan []byte
	amqpConf       amqp.Config
	logger         *logrus.Logger
	established    bool //do connection establish?
	mutex          sync.Mutex
	isInit         bool
}

//Set RabbitMQ
func (rabbit *RabbitMQConfig) SetConsumeMode(msgChan chan []byte) chan []byte {
	rabbit.ConsumeMode = true
	rabbit.ConsumeChan = msgChan
	return msgChan
}
//get *amqp.Connection pointer
func (rabbit *RabbitMQConfig) GetConnectionLink() **amqp.Connection {
	return rabbit.connectionLink
}

func (rabbit *RabbitMQConfig) Init(log *logrus.Logger) {
	rabbit.logger = log
	rabbit.channelLinks = make([]**amqp.Channel, 0, 10)
	rabbit.isInit = true

}

func (rabbit *RabbitMQConfig) IsEstablished() bool {
	return rabbit.established
}

func (rabbit *RabbitMQConfig) getFullAddr() string {
	return fmt.Sprintf("amqp://%s:%s@%s/", rabbit.Login, rabbit.Pass, rabbit.Addr)
}

func (rabbit *RabbitMQConfig) waitConnClosure() {
	log := rabbit.logger
	c := (*rabbit.connectionLink).NotifyClose(make(chan *amqp.Error))
	for msg := range c {
		log.Errorf("%s", msg.Error())
	}
	log.Debug("Rabbit notify chan closed")
}

//method that maintain connection.
func (rabbit *RabbitMQConfig) maintainConn() {
	log := rabbit.logger
	reconnectionTimeout := 7
	if rabbit.ReconnTimeout != 0 {
		reconnectionTimeout = int(rabbit.ReconnTimeout)
	}
	for {
		rabbit.waitConnClosure()
		rabbit.established = false
		counter := 1
		for {
			log.Infof("Try to establish connection #%d", counter)
			conn, err := amqp.DialConfig(rabbit.getFullAddr(), rabbit.amqpConf)
			if err != nil {
				log.Warnf("%v", err)
			} else {
				*(rabbit.connectionLink) = conn
				err := rabbit.QueueDeclare(rabbit.ChanName)
				if err != nil {
					log.Warnf("%v", err)
				} else {
					break
				}
			}
			time.Sleep(time.Second * time.Duration(reconnectionTimeout))
			counter += 1
		}
		rabbit.established = true
		log.Info("connection established")
		chSuccess := true
		for i := 0; i < len(rabbit.channelLinks); i++ {
			ch, err := (*rabbit.connectionLink).Channel()
			if err != nil {
				log.Errorf("Failed to re-create ch: %v", err)
				chSuccess = false
				break
			}
			*(rabbit.channelLinks[i]) = ch
		}
		if !chSuccess { //Suspicious logic. If channel wasn't created, but connection still established, we lose some channels
			continue
		}
		if rabbit.ConsumeMode {
		// Suspicious logic. msg won't delivery If connection established but rabbit.consume() return error
			if err := rabbit.consume(); err != nil {
				log.Errorf("%v", err)
			}
		}
	}
}

//Establish auto-reconnect connection
func (rabbit *RabbitMQConfig) Connect() error {
	if !rabbit.isInit {
		return fmt.Errorf("RabbitMQConfig not initialized(use Init method)")
	}
	log := rabbit.logger
	rabbit.amqpConf = amqp.Config{
		Heartbeat: time.Second * time.Duration(5), //every 5 seconds server check rabbitMQ connection. Server makes 3 attempts to reconnect before close connection
	}
	var err error
	rabbit.connectionLink = new(*amqp.Connection)
	*rabbit.connectionLink, err = amqp.DialConfig(rabbit.getFullAddr(), rabbit.amqpConf)
	if err != nil {
		return fmt.Errorf("Failed to connect to RabbitMQ: %v", err)
	}
	err = rabbit.QueueDeclare(rabbit.ChanName)
	if err != nil {
		return fmt.Errorf("Failed to connect to RabbitMQ: %v", err)
	}
	if rabbit.ConsumeMode {
		err = rabbit.consume()
		if err != nil {
			return err
		}
	}
	rabbit.established = true
	log.Debug("MQ heartbeat time:", (*rabbit.connectionLink).Config.Heartbeat.Seconds())
	go rabbit.maintainConn()
	return nil
}

func (rabbit *RabbitMQConfig) consume() error {
	log := rabbit.logger
	//Channel mustn't close
	ch, err := (*rabbit.connectionLink).Channel()
	if err != nil {
		log.Warnf("failed to create channel: %v", err)
	}
	delivery, err := ch.Consume(rabbit.ChanName, "", true, false, false, false, nil)
	if err != nil {
		log.Warnf("failed to create consumer: %v", err)
	}

	go func() {
		log.Debugf("Delivery chan %s open", rabbit.ChanName)
		for msg := range delivery {
			rabbit.ConsumeChan <- msg.Body
		}
		log.Debugf("Delivery chan closed")
	}()
	return nil
}

//concurrency safety method
//amqp.Channel wrapper
func (rabbit *RabbitMQConfig) Channel() (*amqp.Channel, error) {
	// rabbit.mutex.Lock() internal package method allocateChannel is already concurrency safety
	// defer rabbit.mutex.Unlock()
	return (*rabbit.connectionLink).Channel()
}

//return channel pointer.  Channel will be updated if reconnection happens
func (rabbit *RabbitMQConfig) ChannelLink() (**amqp.Channel, error) {
	// rabbit.mutex.Lock() internal package method allocateChannel is already concurrency safety
	// defer rabbit.mutex.Unlock()
	ch, err := (*rabbit.connectionLink).Channel()
	if err != nil {
		return nil, fmt.Errorf("Failed to open the channel")
	}
	var chLink **amqp.Channel
	chLink = &ch
	rabbit.channelLinks = append(rabbit.channelLinks, chLink)
	return rabbit.channelLinks[len(rabbit.channelLinks)-1], nil
}

func (rabbit *RabbitMQConfig) QueueDeclare(chName string) error {
	ch, err := (*rabbit.connectionLink).Channel()
	if err != nil {
		return fmt.Errorf("Failed to create a RabbitMQ channel: %v", err)
	}
	_, err = ch.QueueDeclare(
		rabbit.ChanName, // name
		false,           // durable
		false,           // delete when unused
		false,           // exclusive
		false,           // no-wait
		nil,             // arguments
	)
	if err != nil {
		return fmt.Errorf("Failed to declare a RabbitMQ queue: %v", err)
	}
	err = ch.Close()
	if err != nil {
		return fmt.Errorf("Failed to close a RabbitMQ channel: %v", err)
	}
	return nil
}
